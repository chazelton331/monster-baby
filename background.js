'use strict';

chrome.tabs.onCreated.addListener(function() {
  if (Math.random() < 0.15) {
    chrome.tabs.create({ url : "monster.html" }, function(tab) {
      var myAudio = new Audio(chrome.runtime.getURL("resources/scream.mp3"));
      myAudio.play();

      setTimeout(function(){
        chrome.tabs.remove(tab.id, function() { });
        myAudio.pause();
      }, 599);
    });
  }
});
